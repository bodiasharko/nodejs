export class CreateFileDto {
  readonly filename: string;
  readonly content: string;
}
