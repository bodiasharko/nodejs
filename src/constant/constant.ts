import { resolve } from 'path';

export const baseFolderURL = resolve(__dirname, '../../files');
export const createdSuccessfully = 'File created successfully';
export const success = 'Success';
export const serverError = 'Server Error';
export const clientError = 'Client Error';
